#ifndef __GUI_PORT_H
#define __GUI_PORT_H

#include "bsp.h"
#include "u8g2.h"

uint8_t STM32_gpio_and_delay(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr);
void u8g2_init(void);
void ShowU8g2Logo(void);


#endif 

