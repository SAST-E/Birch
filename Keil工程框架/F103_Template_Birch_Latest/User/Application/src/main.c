#include "bsp.h"
#include "birch_iic.h"
#include "u8g2.h"
#include "gui_port.h"

extern u8g2_t u8g2; 

int main()
{
	Bsp_Init();
	Birch_IIC_Bus_Init(&birch_iic_bus[1]);
	u8g2_init();
	
	ShowU8g2Logo();
	
	while(1)
	{
		Board_LED_ON();
		Bsp_Delay_ms(500);
		
		Board_LED_OFF();
		Bsp_Delay_ms(500);
	}
	return 0;//程序不会运行到这里
}
