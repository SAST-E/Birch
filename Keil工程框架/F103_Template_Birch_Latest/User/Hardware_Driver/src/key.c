/*
 * @Descripttion: 
 * @version: V0.3
 * @Author: Birch
 * @Date: 2020-03-25 00:42:15
 * @LastEditors: Birch
 * @LastEditTime: 2020-03-30 15:03:25
 * @History: 
 * 0.1 普普通通的按键扫描程序
 * 0.2 采用分层思想进行优化，规范变量命名，但是第一次实现，函数有些冗余
 * 0.3 精简架构，合并功能相近的函数。
 * 
 */

#include "Key.h"

/* 以下是不应在本文件以外被调用的函数*/
/* 函数命名规则为： 模块名_行为 或 模块名_子模块名_行为 */
static void Key_PinInit(Birch_Key_Pin_T *pKeyPin);

static void Key_Mat_WritePinAll(Birch_Key_Pin_T KeyPin[], BitAction BitVal);
static void Key_Mat_WritePinBit(Birch_Key_Pin_T *pKeyPin, BitAction BitVal);

static uint16_t Key_Mat_ReadPinAll(Birch_Key_Pin_T KeyPin[]);

static uint16_t Key_Ind_ReadPinAll(void);

/* 存储矩阵键盘输入端引脚信息的结构体数组 */
/* 输入引脚设置为下拉输入模式 */
static Birch_Key_Pin_T s_KeyMatPinIN[] = {
	{.GPIOx = GPIOB, .Pin = GPIO_Pin_12, .Mode = GPIO_Mode_IPD},
	{.GPIOx = GPIOB, .Pin = GPIO_Pin_13, .Mode = GPIO_Mode_IPD},
	{.GPIOx = GPIOB, .Pin = GPIO_Pin_14, .Mode = GPIO_Mode_IPD},
	{.GPIOx = GPIOB, .Pin = GPIO_Pin_15, .Mode = GPIO_Mode_IPD},
};

/* 存储矩阵键盘输出端引脚信息的结构体数组 */
/* 输出引脚设置为推挽模式 */
static Birch_Key_Pin_T s_KeyMatPinOut[] = {
	{.GPIOx = GPIOA, .Pin = GPIO_Pin_11, .Mode = GPIO_Mode_Out_PP},
	{.GPIOx = GPIOA, .Pin = GPIO_Pin_12, .Mode = GPIO_Mode_Out_PP},
	{.GPIOx = GPIOB, .Pin = GPIO_Pin_6, .Mode = GPIO_Mode_Out_PP},
};

/* 存储矩阵键盘输入端、输出端引脚数目的静态变量 */
static uint8_t s_KeyMatPinNumIN  = 0;
static uint8_t s_KeyMatPinNumOut = 0;

/* 记录独立按键引脚信息的结构体数组 */
static Birch_Key_Pin_T s_KeyIndPin[] = {
	{.GPIOx = GPIOA, .Pin = GPIO_Pin_0, .Mode = GPIO_Mode_IPU},
	{.GPIOx = GPIOA, .Pin = GPIO_Pin_1, .Mode = GPIO_Mode_IPU},
	{.GPIOx = GPIOA, .Pin = GPIO_Pin_2, .Mode = GPIO_Mode_IPU},
};

/* 存储独立按键引脚数目的静态变量 */
static uint8_t s_KeyIndPinNum = 0;


/**
 * @brief: 初始化按键所连接的引脚为指定的模式
 * @example: Key_PinInit(&s_KeyMAtPinIN[2]);
 * @param:   pKeyPin是一个Birch_Key_Pin_T类型的指针，故传入的参数应为该类型对象的地址
 * @return:  None
 */
void Key_PinInit(Birch_Key_Pin_T *pKeyPin)
{
	GPIO_InitTypeDef GPIOInitStructrue;
	RCC_APB2PeriphClockCmd((uint32_t)(1<<(((uint32_t)pKeyPin->GPIOx - APB2PERIPH_BASE)>>10)) \
		,ENABLE);
	
	GPIOInitStructrue.GPIO_Mode  = pKeyPin->Mode;
	GPIOInitStructrue.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOInitStructrue.GPIO_Pin   = pKeyPin->Pin;
	
	GPIO_Init(pKeyPin->GPIOx, &GPIOInitStructrue);
}

/**
 * @brief: 初始化矩阵键盘所连接的引脚
 * @example: None
 * @param:   None
 * @return:  None
 */
void Key_Mat_Init(void)
{
	/* 获取矩阵键盘输入端引脚的数目 */
	s_KeyMatPinNumIN = sizeof(s_KeyMatPinIN) / sizeof(s_KeyMatPinIN[0]);

	for(uint8_t i=0;i<s_KeyMatPinNumIN;i++) {
		Key_PinInit(&s_KeyMatPinIN[i]);
	}

	/* 获取矩阵键盘输出端引脚的数目 */
	s_KeyMatPinNumOut = sizeof(s_KeyMatPinOut) / sizeof(s_KeyMatPinOut[0]);

	for(uint8_t i=0;i<s_KeyMatPinNumOut;i++) {
		Key_PinInit(&s_KeyMatPinOut[i]);
	}
}

/**
 * @brief: 将某个Birch_Key_Pin_T类型的列表中的所有引脚对象设置为指定的模式
 * @example: Key_Mat_WritePinAll(s_KeyMatPinOut, Bit_RESET);
 * @param:   BitAction是一个枚举类型，其值应为Bit_SET(1)或Bit_RESET(0)
 * @return:  None
 */
void Key_Mat_WritePinAll(Birch_Key_Pin_T KeyPin[], BitAction BitVal)
{
	for(uint8_t i=0;i<s_KeyMatPinNumOut;i++) {
		GPIO_WriteBit(KeyPin[i].GPIOx, KeyPin[i].Pin, BitVal);
	}
}

/**
 * @brief: 将指定的某个Birch_Key_Pin_T类型的引脚对象设置为指定的模式
 * @example: Key_Mat_WritePinBit(&s_KeyMatPinOut[2], Bit_SET);
 * @param:   pKeyPin是一个Birch_Key_Pin_T类型的指针，故传入的参数应为该类型对象的地址,
 * 			 BitAction是一个枚举类型，其值应为Bit_SET(1)或Bit_RESET(0)
 * @return:  None
 */
void Key_Mat_WritePinBit(Birch_Key_Pin_T *pKeyPin, BitAction BitVal)
{
	GPIO_WriteBit(pKeyPin->GPIOx, pKeyPin->Pin, BitVal);
}

/**
 * @brief: 读取矩阵键盘输入端所有引脚的状态，并返回键值
 * @example: Key_Mat_ReadPinAll(s_KeyMatPinIN);
 * @param:   
 * @return:  返回值的最低位状态对应最后一次读取的按键键值，依此类推
 */
uint16_t Key_Mat_ReadPinAll(Birch_Key_Pin_T KeyPin[])
{
	uint16_t key_value = 0;
	for(uint8_t i=0;i<s_KeyMatPinNumIN;i++)
	{
		key_value <<= 1;
		key_value += GPIO_ReadInputDataBit(KeyPin[i].GPIOx, KeyPin[i].Pin)?1:0;
	}
	return key_value;
}

/**
 * @brief: 扫描矩阵按键，若有按键按下则返回键值
 * @example: ulKeyValue = Key_Mat_Scan();
 * @param:   None
 * @return:  返回值的最低位状态对应最后一次读取的按键键值，依此类推
 */
uint32_t Key_Mat_Scan(void)
{
	uint32_t ulKeyValue = 0;
	Key_Mat_WritePinAll(s_KeyMatPinOut, Bit_SET);

	if(!Key_Mat_ReadPinAll(s_KeyMatPinIN)) { //若无按键按下
		return 0;
	}
	else 
	{
		Bsp_Delay_ms(20); //延时消抖
		if(Key_Mat_ReadPinAll(s_KeyMatPinIN)) //若有按键按下
		{ 
			for(uint8_t i=0;i<s_KeyMatPinNumOut;i++) //依次拉高每一路输出端进行扫描
			{
				/* 除指定引脚外输出端全部置低，指定引脚拉高 */
				Key_Mat_WritePinAll(s_KeyMatPinOut, Bit_RESET); 
				Key_Mat_WritePinBit(&s_KeyMatPinOut[i], Bit_SET);
				
				for(uint8_t j=0;j<s_KeyMatPinNumIN;j++) {
					ulKeyValue <<=1;
					ulKeyValue += GPIO_ReadInputDataBit(s_KeyMatPinIN[j].GPIOx, \
					s_KeyMatPinIN[j].Pin)?1:0;
					
				}
			}
		}
	}
	return ulKeyValue;
}

/**
 * @brief: 初始化独立按键所连接的引脚
 * @example: None
 * @param:   None
 * @return:  None
 */
void Key_Ind_Init(void)
{
	GPIO_InitTypeDef GPIOInitStructrue;

	/* 获取独立按键的数目 */
	s_KeyIndPinNum = sizeof(s_KeyIndPin) / sizeof(s_KeyIndPin[0]);

	for(uint8_t i=0;i<s_KeyIndPinNum;i++)
	{
		RCC_APB2PeriphClockCmd((uint32_t)(1<<(((uint32_t)s_KeyIndPin[i].GPIOx - APB2PERIPH_BASE)>>10)) \
		,ENABLE);
		
		GPIOInitStructrue.GPIO_Mode  = s_KeyIndPin[i].Mode;
		GPIOInitStructrue.GPIO_Speed = GPIO_Speed_50MHz;
		GPIOInitStructrue.GPIO_Pin   = s_KeyIndPin[i].Pin;
		
		GPIO_Init(s_KeyIndPin[i].GPIOx, &GPIOInitStructrue);
	}
	
}

/**
 * @brief: 读取所有独立按键所连接的引脚的状态，并返回相应值
 * @example: None
 * @param:   None
 * @return:  返回值的最低位状态对应最后一次读取的按键键值，依此类推
 */
uint16_t Key_Ind_ReadPinAll(void)
{
	uint16_t usKeyValue = 0;
	for(uint8_t i=0;i<s_KeyIndPinNum;i++) {
		usKeyValue <<= 1;
		usKeyValue += GPIO_ReadInputDataBit(s_KeyIndPin[i].GPIOx, s_KeyIndPin[i].Pin)?1:0;
	}
	return usKeyValue;
}

/**
 * @brief: 扫描所有独立按键，并返回一个值
 * @example: None
 * @param:   None
 * @return:  返回值的最低位状态对应最后一次读取的按键键值，依此类推
 */
uint16_t Key_Ind_Scan(void)
{
	uint16_t usKeyValue = 0;
	if(Key_Ind_ReadPinAll()) {//若有按键按下
		Bsp_Delay_ms(20);
		usKeyValue = Key_Ind_ReadPinAll();
		if(0 != usKeyValue) {//若仍有按键按下
			return usKeyValue;
		}
	}
	return 0;
}




/**
 *    _______   __________    __   _      __   __  _             __  __         __  __                  __   __    __
 *   / __/ _ | / __/_  __/   / /  (_)__ _/ /  / /_(_)__  ___ _  / /_/ /  ___   / /_/ /  ___  __ _____ _/ /  / /_  / /
 *  _\ \/ __ |_\ \  / /     / /__/ / _ `/ _ \/ __/ / _ \/ _ `/ / __/ _ \/ -_) / __/ _ \/ _ \/ // / _ `/ _ \/ __/ /_/ 
 * /___/_/ |_/___/ /_/     /____/_/\_, /_//_/\__/_/_//_/\_, /  \__/_//_/\__/  \__/_//_/\___/\_,_/\_, /_//_/\__/ (_)  
 *                                /___/                /___/                                    /___/                
 *                     
 */
