# 白桦系列开发板是什么？

&emsp;&emsp;白桦系列开发板是南京邮电大学校科协电子部成员为学习STM32的萌新们设计的一款“模块化”的开发板，它是一个任何人都能轻松搭建的平台，成本极低，但功能却不少。同时，我们正致力于以这块小小的开发板为基础，设计更多新手友好的硬件平台，敬请关注！

&emsp;&emsp;关于开发板的更多信息，请戳这里：  （目前还没有呢）

&emsp;&emsp;另外，这里附一张开发板靓照。锵锵！

![](./原理图、3D预览、实物图/实物图-正面.jpg)

# 使用教程

&emsp;&emsp;开发板配套软件教程锐意制作中！

* 准备工作：（还没有呢）
* [建立工程模板](https://segmentfault.com/a/1190000022411447)
* [完善工程模板](https://segmentfault.com/a/1190000022425826)





## 用开发板创建好玩的东西

* [永远准时的WiFi时钟](https://segmentfault.com/a/1190000022148790?_ea=38564100)
* [控制LED的上位机](https://segmentfault.com/a/1190000022704635)
* （这里还没有东西呢）

## 开发板的一些“扩展部件”

* [Birch开发板配套的盒子](https://gitee.com/multicoloredstone/ExtensionForBirch/tree/master/Box)
* 

## 本开发板适用的一些科协工具

* [推荐使用这个调试器](https://gitee.com/SAST-E/CMSIS-DAP-STM32/tree/master)
* [推荐使用这个逻辑分析仪](https://gitee.com/SAST-E/miniLA)

# 补充

&emsp;&emsp;有小伙伴表示使用chrome下载zip得到的是一个很小的html文件，对于这种情况，建议使用edge试一下。当然，绝对不会翻车的方法是：去[这里](https://git-scm.com/downloads)下一个git然后花几分钟看下[新手教程](https://www.liaoxuefeng.com/wiki/896043488029600/898732792973664)搞清楚怎么开git bash，git clone什么意思之后执行以下指令克隆仓库到本地。

```
git clone https://gitee.com/SAST-E/Birch.git
```

