#include "stm32f10x.h"
#include "stm32f10x_conf.h"

void delay_ms_test(uint32_t delay_time);

void Board_LED_Init(void);
void Board_LED_ON(void);
void Board_LED_OFF(void);

int main()
{
	Board_LED_Init();
	
	while(1)
	{
		Board_LED_ON();
		
		delay_ms_test(200);
		
		Board_LED_OFF();
		
		delay_ms_test(200);
	}
	return 0;//程序不会运行到这里
}

void delay_ms_test(uint32_t delay_time)
{
	for(int i=0;i<delay_time;i++) 
	{
		for(int j=0;j<11450;j++) 
		{
			__nop();//do nothing
		}
	}
}

void Board_LED_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
}

void Board_LED_ON(void)
{
	GPIO_WriteBit(GPIOC,GPIO_Pin_13,0);
}

void Board_LED_OFF(void)
{
	GPIO_WriteBit(GPIOC,GPIO_Pin_13,1);
}