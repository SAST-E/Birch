#ifndef __BSP_UART_H
#define __BSP_UART_H

#include "stdio.h"
#include "stm32f10x.h"
#include "stm32f10x_conf.h"

#include "bsp_led.h"
#include "bsp_timer.h"

typedef struct {
	USART_TypeDef *USARTx;
	uint32_t RCC_Periph_Base; //eg: RCC_APB2Periph_USART1
	
	GPIO_TypeDef *TX_GPIOx;
	uint16_t TX_Pin;
	
	GPIO_TypeDef *RX_GPIOx;
	uint16_t RX_Pin;
	
	uint32_t baud;	//波特率，常取115200或9600
	uint8_t  IRQChannel;
	
}Birch_UART_T;

extern Birch_UART_T g_UartDevice[];

extern uint8_t  usart1_rx_buffer[];
extern uint8_t  usart1_rx_ok;//接收完成标识
extern uint16_t usart1_rx_count;

void Uart_Init(Birch_UART_T *pUART);//

#endif


