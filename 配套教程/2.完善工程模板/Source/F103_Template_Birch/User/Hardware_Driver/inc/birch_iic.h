/*
 * @Descripttion: 
 * 由某位无聊的白桦同志通过根部的真菌侵入网线破解了我家的网络后在我的电脑上随机生成的
 * 一份用于模拟IIC驱动的代码，我看了一下，貌似写得比我好，还可以很方便地管理多条IO模拟
 * 的IIC总线，于是就拿出来分享一下。
 * 此文件由@南京邮电大学校科协电子部创建，仅供学习交流，不可用于其他用途。
 * @version: V0.2
 * @Author: Birch(路边白桦)
 * @Date: 2020-03-18 16:02:42
 * @LastEditors: Birch
 * @LastEditTime: 2020-03-21 16:57:15
 */

#ifndef __BIRCH_IIC_H
#define __BIRCH_IIC_H

#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "bsp.h"


#define BIRCH_IIC_STATUS_FAILED  	1		//IIC通信状况
#define BIRCH_IIC_STATUS_OK			0

#define BIRCH_IIC_ACK	1		//用于决定是否发送响应，示意从机继续发送
#define BIRCH_IIC_NACK 	0


#define BIRCH_IIC_SCL_H(bus) Birch_IIC_Set_Pin((bus)->SCL_GPIOx,(bus)->SCL_Pin)
#define BIRCH_IIC_SCL_L(bus) Birch_IIC_Reset_Pin((bus)->SCL_GPIOx,(bus)->SCL_Pin)

#define BIRCH_IIC_SDA_H(bus) Birch_IIC_Set_Pin((bus)->SDA_GPIOx,(bus)->SDA_Pin)
#define BIRCH_IIC_SDA_L(bus) Birch_IIC_Reset_Pin((bus)->SDA_GPIOx,(bus)->SDA_Pin)

#define BIRCH_IIC_READ_SDA(bus) GPIO_ReadInputDataBit((bus)->SDA_GPIOx,(bus)->SDA_Pin)


typedef struct 
{
	GPIO_TypeDef *SCL_GPIOx;
	uint16_t SCL_Pin;
	
	GPIO_TypeDef *SDA_GPIOx;
	uint16_t SDA_Pin;
	
	uint8_t Speed_range;	//速率等级,值越小速率越高
	
}Birch_IIC_Bus_T;

extern Birch_IIC_Bus_T birch_iic_bus[];

void Birch_IIC_Delay(Birch_IIC_Bus_T *bus);
void Birch_IIC_Set_Pin(GPIO_TypeDef *GPIOx,uint16_t Pin);
void Birch_IIC_Reset_Pin(GPIO_TypeDef *GPIOx,uint16_t Pin);
void Birch_IIC_Pin_Init(GPIO_TypeDef *GPIOx,uint16_t Pin);


void Birch_IIC_Bus_Init(Birch_IIC_Bus_T *bus);
void Birch_IIC_Start(Birch_IIC_Bus_T *bus);
void Birch_IIC_Stop(Birch_IIC_Bus_T *bus);
uint8_t Birch_IIC_WaitAck(Birch_IIC_Bus_T *bus);
void Birch_IIC_Send_Ack(Birch_IIC_Bus_T *bus);
void Birch_IIC_Send_NAck(Birch_IIC_Bus_T *bus);
void Birch_IIC_Base_Send_Byte(Birch_IIC_Bus_T *bus, uint8_t data);
uint8_t Birch_IIC_Base_Read_Byte(Birch_IIC_Bus_T *bus,uint8_t Ack);	//Ack参数填BIRCH_IIC_ACK/NACK


#endif  


/**
 * 温馨提示：如何使用本文件？
 * 最重要的当然是遵循IIC通信的时序和所要驱动的设备的手册给出的规则。
 * 发送起始条件，发送地址和读写位，然后开始一个或若干个数据的传送，
 * 读取时也是类似的，根据剩余的需要读取的数据的个数来决定是否需要发
 * 送应答。除了这些基本的，还要注意你的设备有没有什么奇怪的规定，比
 * 如发送读请求之后经过若干时间才能读取之类的。
 * 祝你好运！
 * 
 *
 */
