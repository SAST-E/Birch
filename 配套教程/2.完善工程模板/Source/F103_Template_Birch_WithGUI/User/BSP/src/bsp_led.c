/*
 * @Descripttion: 
 * @version: V0.1
 * @Author: Birch
 * @Date: 2020-03-18 16:02:42
 * @LastEditors: Birch
 * @LastEditTime: 2020-03-27 18:25:58
 * @History: 
 */
#include "bsp_led.h"
  


/**
 * @brief: Initial the pin of led on coreboard
 * @example: None
 * @param:  None 
 * @return: None
 */
void Board_LED_Init(void)
{
	RCC_APB2PeriphClockCmd((uint32_t)(1<<(((uint32_t)BOARD_LED_PORT - APB2PERIPH_BASE)>>10)) \
	,ENABLE);
	
	GPIO_InitTypeDef GPIOInitStructure;
	GPIOInitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIOInitStructure.GPIO_Pin = BOARD_LED_PIN;
	GPIOInitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(BOARD_LED_PORT,&GPIOInitStructure);
}

/**
 * @brief: Turn on the led on coreboard
 * @example: None
 * @param: 	None
 * @return: None
 */
void Board_LED_ON(void)
{
	GPIO_WriteBit(BOARD_LED_PORT,BOARD_LED_PIN,0);
}

/**
 * @brief: Turn off the led on coreboard
 * @example: None
 * @param:   None
 * @return:  None
 */
void Board_LED_OFF(void)
{
	GPIO_WriteBit(BOARD_LED_PORT,BOARD_LED_PIN,1);
}

/**
 * @brief: 
 * @example: None
 * @param:   
 * @return:  None
 */
void Board_LED_Blink(uint8_t times,uint16_t delay_time_ms)
{
	for(uint8_t i=0;i<times;i++)
	{
		Board_LED_ON();
		
		Bsp_Delay_ms(delay_time_ms);
		
		Board_LED_OFF();
		
		Bsp_Delay_ms(delay_time_ms);
	}
}







