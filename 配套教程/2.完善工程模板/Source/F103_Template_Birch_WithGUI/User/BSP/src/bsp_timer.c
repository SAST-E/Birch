#include "bsp_timer.h"


void Bsp_Delay_Init(void)
{
	SysTick_Config(SystemCoreClock/1000/8);
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);
	
}

void Bsp_Delay_us(uint32_t _time)
{
	SysTick->LOAD = 72*_time;
	SysTick->CTRL =SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_CLKSOURCE_Msk;
	while(!(SysTick->CTRL&SysTick_CTRL_COUNTFLAG_Msk));
	SysTick->CTRL =~SysTick_CTRL_ENABLE_Msk;
	
}

void Bsp_Delay_ms(uint16_t _time)
{
	uint16_t time = _time;
	do {
		Bsp_Delay_us(1000);
	}while(time--);
	
}
